from setuptools import setup, find_packages

dsc = """Extension of icalendar.Calendar class with a pass property
and display() and write() methods"""

setup(name="icalendar_plus",
      version="0.1",
      description=dsc,
      author="Matthieu Leclair",
      author_email="matthieu.leclair@env.ethz.ch",
      install_requires=['icalendar >= 3.0'],
      packages=['icalendar_plus']
)
