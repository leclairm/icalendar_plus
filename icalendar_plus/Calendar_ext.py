import os
from icalendar import Calendar


class Calendar_ext():
    """Class composed of the icalendar.Calendar class
    adding a path property and write and display methods"""

    def __init__(self, path, reset=False, summary=None):

        self.path = path

        if reset or not os.path.exists(self.path):
            self.calendar = Calendar()
            if summary is not None:
                self.calendar['summary'] = str(summary)
        else:
            if os.path.exists(self.path):
                with open(self.path, 'rb') as cal_file:
                    content = cal_file.read()
                    if content:
                        self.calendar = Calendar.from_ical(content)
                    else:
                        raise ValueError('File {:s} is empty, cannot parse as calendar object'.format(path))
            else:
                raise ValueError('File {:s} does not exist, cannot parse as calendar object'.format(path))

    # Properties
    # ----------
    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = os.path.normpath(path)
        
    # Methods
    # -------
    def write(self, permissions=None):
        with open(self.path, 'wb') as cal_file:
            cal_file.write(self.calendar.to_ical())
        if permissions is not None:
            os.chmod(self.path, permissions)

    def chmod(self, permissions):
        os.chmod(self.path, permissions)

    def display(self):
        print(self.calendar.to_ical().decode('utf8').replace('\r\n', '\n').strip())

    def add_component(self, comp):
        self.calendar.add_component(comp)
